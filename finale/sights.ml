(** Generates the sat*times at which a satellite will have a point in sight,
	ordered by time *)

open Sat
open Pos

let nextSight maxTime sat (a,b,c,d) =
(*	Format.eprintf "\t(%d %d %d %d) - given %ds@." a b c d maxTime ; *)
	Sat.next_sight sat a b c d maxTime

let iterSights maxTime pos sats(*const*) =
	let mkRect sat =
		pos.lat - sat.d, pos.long - sat.d, pos.lat + sat.d, pos.long + sat.d in
	let nSights = Array.init (Array.length sats)
		(fun x -> nextSight maxTime sats.(x) (mkRect sats.(x))) in
	let arrayArgMin arr = CCArray.foldi (fun cur i x ->
		if x < arr.(cur)
			then i
			else cur) 0 arr
		in
	(fun () ->
		let argMin = arrayArgMin nSights in
		let cTime = nSights.(argMin) in
		if cTime < maxTime then begin
			let rect = mkRect sats.(argMin) in
			let nSat = n_turns sats.(argMin) cTime in
			nSights.(argMin) <- cTime + 1 +
				(nextSight (maxTime-cTime-1) (next_position nSat) rect);
			Some (nSat, cTime)
		end else
			None)
