(* A position map.
   Abstraction: give a list of points in an interval *)
open Pos

let latrange = 324000
let longrange = 648000
let blocksize = 10000

let nlat = (2*latrange + blocksize - 1)/blocksize
let nlong = (2 * longrange + blocksize)/blocksize
let merge x y= x*nlong+y

type 'a posmap = {
  map : 'a list array;
}

let coords_of p = ((latrange + p.lat) / blocksize), ((longrange + p.long) / blocksize)
let block_of p =
  merge ((latrange + p.lat) / blocksize) ((longrange + p.long) / blocksize)

let map_make () =
  { map = Array.make (nlat * nlong) [] }

let map_insert map point data =
  let block = block_of point in
  map.map.(block) <- data::map.map.(block)

let minmax x y = min x y, max x y
let prefilter_interval map xup xdown yup ydown =
  let (x1,y1) = coords_of {lat = xup; long =  yup} in
  let (x2,y2) = coords_of {lat = xdown; long =  ydown} in
  let (x1, x2) = minmax x1 x2 in
  let (y1,y2) = minmax y1 y2 in
  let rets = ref [] in
  for i = x1 to x2 do
    for j = y1 to y2 do
      rets :=  merge i j :: !rets;
    done;
  done;
  List.concat (List.map (fun b -> map.(b)) !rets)


