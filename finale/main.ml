
module G = Greedy_col

(*
let () =
  let sat = Sat.{ pos = { Pos.lat = 32; Pos.long = 54 } ;
				  speed = -142 ; w = 0; d = 0 ; dir = {Pos.lat = 0; Pos.long = 0}} in
  for i = 0 to 100 do
	assert (Sat.n_turns sat (i * 100) = Sat.n_pos_debug sat (i * 100))
  done;
  Format.eprintf "ok@."
*)

let () =
  (*
  let data = Input.read_pb () in
  let dataByLoc = Overlaps.sortByLoc data in
  let imgMap =
    let map = Posmap.map_make () in
    Array.iteri (fun icol col ->
                 Array.iteri (fun iimg pos ->
                              Posmap.map_insert map pos (icol, iimg)
                             ) col.Col.imgs) data.collections;
    map
  in
  Greedy.greedy_scan imgMap data *)
  
  let pb = Input.read_pb () in
  Greedy_col.try_greedy Sys.argv.(1) pb

