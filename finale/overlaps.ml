(****
	Renvoie une liste de photos présentes dans deux collections
	(ie optimisables en une seule)
****)

open Col
open Pos
open Input

let subsetLists l = 
	let rec aux cur = function
	| [] -> (match cur with
		| [] -> []
		| cur -> [cur])
	| hd::tl -> (aux (hd::cur) tl) @ (aux cur tl)
	in
	let lists = ref (aux [] l) in
	(fun () -> match !lists with
		| [] -> None
		| hd::tl -> lists := tl ;
			Some hd)


let sortByLoc (data : Input.t)=
	let nbImg = Array.fold_left (fun cur x -> cur + Array.length (x.imgs)) 0
		data.collections in
	let outCollec = Array.make nbImg {
		loc = (data.collections.(0).imgs.(0)) ;
		colId = 0;
		imgId = 0
		} in
	let curPos = ref 0 in
	Array.iteri (fun iCol col ->
		Array.iteri (fun iImg img ->
			outCollec.(!curPos) <- {
				loc = img ;
				colId = iCol ;
				imgId = iImg
			} ;
			curPos := !curPos + 1) col.imgs) data.collections ;
	Array.sort (fun iloc1 iloc2 -> match iloc1.loc.lat - iloc2.loc.lat with
		| 0 -> iloc1.loc.long - iloc2.loc.long
		| n -> n)
		outCollec ;
	outCollec

let emptyRange (rg:range) = rg.stop < rg.start

let rangesInter (rg1:range) (rg2:range) = {
	start = max rg1.start rg2.start ;
	stop = min rg1.stop rg2.stop }

let rangeColInter (rg1 : range array) (rg2: range array) =
	let pos2 = ref 0 in
	let a2len = Array.length rg2 in
	Array.fold_left (fun cur rg ->
		let out = ref cur in
		if !pos2 < 0 then pos2 := 0;
		while !pos2 < a2len && rg2.(!pos2).stop < rg.start do
			pos2 := !pos2 + 1
		done;
		while !pos2 < a2len && (not (emptyRange (rangesInter rg rg2.(!pos2))));
			do
			out := (rangesInter rg rg2.(!pos2))::!out ;
			pos2 := !pos2 + 1
		done;
		pos2 := !pos2-1;
		!out) [] rg1

let rangeColInterN = function
| [] -> failwith "Empty list"
| hd::[] -> Array.to_list hd
| hd::tl -> List.fold_left (fun x -> rangeColInter (Array.of_list x))
	(Array.to_list hd) tl

let argmax score vals =
	let rec aux cmax carg = function
	| [] -> carg
	| hd::tl ->
		let sc = score hd in
		if cmax < sc then
			aux sc hd tl
		else
			aux cmax carg tl
	in
	(match vals with
	| [] -> failwith "Empty list"
	| hd::tl -> aux (score hd) hd tl)

let listMinus l1 l2 =
	List.fold_left (fun cur x ->
		if List.mem x l2
			then cur
			else x::cur) [] l1

let collOverlap data locSort =
	let eqLoc x1 x2 = x1.loc.lat = x2.loc.lat && x1.loc.long = x2.loc.long in
	(** Returns a list of images overlapping between collections *)
	let locOverlaps = Array.fold_left (fun cur x -> match cur with
		| [] -> [[x]]
		| []::tl -> failwith "Dafuq"
		| (hd::tl)::coltl ->
			if eqLoc x hd then
				(x::hd::tl)::coltl
			else
				[x]::(hd::tl)::coltl
		) [] locSort in

	let rec subsetLists cur = function
	| [] -> (match cur with
		| [] -> []
		| cur -> [cur])
	| hd::tl -> (subsetLists (hd::cur) tl) @ (subsetLists cur tl)
	in
	let rec optiOverlaps cur (overl:Input.imgLoc list) =
		let score colls =
			let rangelist = List.map (fun x ->
				data.collections.(x.colId).ranges) colls in
			let range = rangeColInterN rangelist in
			if range = [] then
				0.
			else
				List.fold_left (fun cur x ->
					let col = data.collections.(x.colId) in
					cur +. (float_of_int col.score) /.
						(float_of_int (Array.length col.imgs))) 0. colls
		in
		(match overl with
		| [] -> cur
		| overl ->
			let amax = argmax score (subsetLists [] overl) in
			(optiOverlaps (amax::cur) (listMinus overl amax)))
		in
	List.fold_left (fun prev cur ->
		(optiOverlaps [] cur) @ prev) [] locOverlaps
