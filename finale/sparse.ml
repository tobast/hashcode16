(** Allows to map, for a satellite, a time to a photograph position, and
	find efficiently if it is possible to photograph [pos] at [time] with
	the previously set constraints *)

open Pos
open Sat

module TMap = Map.Make(struct type t = int let compare = compare end)
type t = Pos.t TMap.t

exception PicAlreadyDone of int * Pos.t

let empty = TMap.empty

let locEquals l1 l2 =
	l1.lat = l2.lat && l1.long = l2.long

let deg = 3600
let posDiff p1 p2 = { lat=p1.lat-p2.lat ;
	long = (p1.long-p2.long + (180+360)*deg) mod (360*deg) - 180*deg }

let canBindAt sat pos satConstraints time =

	let canMoveOf mv time =
		abs mv.lat <= time * sat.w && abs mv.long <= time * sat.w in

	match TMap.split time satConstraints with
	| _,Some bPos,_ ->
		(*
		if locEquals pos bPos then
			raise (PicAlreadyDone (time,pos))
		else *)
			false
	| prev,_,next ->
		let befTime,befPos = (try TMap.max_binding prev
			with Not_found -> 0,{lat=0;long=0}) in
		let aftTime,aftPos = (try TMap.min_binding next
			with Not_found -> 800000,{lat=0;long=0}) in
		(canMoveOf (posDiff pos befPos) (time-befTime)) &&
			(canMoveOf (posDiff aftPos pos) (aftTime-time))
		
	

let bindTime sat imgLoc satConstraints time =
	let pos = posDiff imgLoc sat.pos in
	assert (abs pos.lat <= sat.d && abs pos.long <= sat.d);

	match canBindAt sat pos satConstraints time with
	| false -> None
	| true -> Some (TMap.add time pos satConstraints)
