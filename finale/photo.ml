
let possible input =
  (* let all_photos =
     Array.concat (Array.to_list (
     Array.mapi (fun i col ->
     Array.mapi (fun j photo -> (photo, i, j)) col.Col.imgs)
     input.Input.collections))
     in
     Array.sort compare all_photos;
  *)
  let in_range s p =
    let dlat = s.Sat.pos.Pos.lat - p.Pos.lat in
    let dlong = Sat.longitude (s.Sat.pos.Pos.long - p.Pos.long) in
    (abs dlat <= s.Sat.d) && (
      dlong <= s.Sat.d || (Sat.longitude (-dlong)) <= s.Sat.d
    )
  in
  let sats = Array.copy input.Input.sats in
  (* let result = Array.map (fun _ -> []) sats in *)
  let result = Array.map (fun _ -> Hashtbl.create 17) sats in
  let add_range htbl key time =
    if Hashtbl.mem htbl key then
      match Hashtbl.find htbl key with
      | (a, b) :: r when b = time - 1 ->
        Hashtbl.replace htbl key ((a, time) :: r)
      | r -> Hashtbl.replace htbl key ((time, time) :: r)
    else
      Hashtbl.add htbl key [(time, time)]
  in
  let range_index = Array.map (fun _ -> 0) input.Input.collections in
  for t = 0 to input.Input.max_time - 1 do
    if (t land 0xff) = 0 then
      Format.eprintf "%d / %d@." t input.Input.max_time;
    for c = 0 to Array.length input.Input.collections - 1 do
      let col = input.Input.collections.(c) in
      if range_index.(c) < Array.length col.Col.ranges &&
         col.Col.ranges.(range_index.(c)).Col.stop < t then
        range_index.(c) <- range_index.(c) + 1;
      if range_index.(c) < Array.length col.Col.ranges &&
         col.Col.ranges.(range_index.(c)).Col.start <= t then begin
        Array.iteri (fun i photo ->
            Array.iteri (fun s sat ->
                if in_range sat photo then begin
                  (* result.(s) <- (c, i, t) :: result.(s) *)
                  add_range result.(s) (c, i) t
                end
              ) sats
          ) col.Col.imgs
      end
    done;
    for i = 0 to Array.length sats - 1 do
      sats.(i) <- Sat.next_position sats.(i)
    done
  done;
  result

let read file =
  let ch = open_in file in
  (Marshal.from_channel ch : (int * int, (int * int) list) Hashtbl.t)

