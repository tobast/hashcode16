
open Col
open Pos
open Sat
open Input

type img = {
  id : int;
  time : int;
  loc : Pos.t;
}

type t = img array

let print t =
  Format.printf "%d@\n" (Array.length t);
  Array.iter (fun img ->
      Format.printf "%d %d %d %d@\n"
        img.loc.lat img.loc.long img.time img.id) t

type sat = {
  sat : Sat.t;
  mutable last_picture : int;
  mutable last_orientation : Pos.t;
}

type state = {
  sats : sat array;
  cols : bool array array;
}

let score_step input state round score img  =
  if img.time = round then begin
    let sat = state.sats.(img.id) in
    let lat_diff = img.loc.lat - sat.sat.pos.lat in
    let long_diff = img.loc.long - sat.sat.pos.long in
    assert (abs lat_diff < sat.sat.d);
    assert (abs (lat_diff - sat.last_orientation.lat) < (sat.sat.w * (round - sat.last_picture)));
    assert (abs long_diff < sat.sat.d);
    assert (abs (long_diff - sat.last_orientation.long) < (sat.sat.w * (round - sat.last_picture)));
    sat.last_picture <- round;
    sat.last_orientation <- { lat = lat_diff; long = long_diff; };
    let tmp = ref 0 in
    Array.iteri (fun i col ->
        if CCArray.exists (fun r -> r.start <= round && round <= r.stop) col.ranges then begin
          Array.iteri (fun j p ->
              if Pos.equal p img.loc then begin
                if not state.cols.(i).(j) then begin
                  state.cols.(i).(j) <- true;
                  if CCArray.for_all (fun x -> x) state.cols.(i) then
                    tmp := !tmp + input.collections.(i).score
                end
              end) col.imgs
        end
      ) input.collections;
    score + !tmp
  end else
    score

let rec score_aux input state output score round =
  if round >= input.max_time then score
  else begin
    let new_score = Array.fold_left (score_step input state round) score output in
    Array.iteri (fun i sat ->
        state.sats.(i) <- { sat with sat = Sat.next_position sat.sat }) state.sats;
    score_aux input state output new_score (round + 1)
  end

let score (input :Input.t) output =
  let state = {
    sats = Array.map (fun sat ->
        { sat; last_picture = 0; last_orientation = sat.dir; }) input.sats;
    cols = Array.init (Array.length input.collections) (fun i ->
        Array.make (Array.length input.collections.(i).imgs) false);
  } in
  score_aux input state output 0 0


