
open Sat
open Col
open Pos
open Input

module M = Map.Make(struct type t = Pos.t let compare = Pervasives.compare end)

let rec greedy_img state c m img iter =
  match iter () with
  | None -> raise Exit
  | Some (sat, round) ->
    if Col.in_ranges round c.ranges then
      begin match Sparse.bindTime sat img state.(sat.id) round with
        | None ->
          greedy_img state c m img iter
        | Some cstr ->
          state.(sat.id) <- cstr;
          M.add img (sat.id, round) m
      end
    else begin
      greedy_img state c m img iter
    end

let greedy_col max_time sats state (m, score) c =
  let backup = Array.copy state in
  try
    let m' = Array.fold_left (fun m img ->
        if M.mem img m then m
        else begin
          let next = Sights.iterSights max_time img sats in
          greedy_img state c m img next
        end) m c.imgs in
    m', score + c.score
  with Exit ->
    Array.blit backup 0 state 0 (Array.length backup);
    m, score

let score c =
  let l = Array.length c.imgs in
  (float c.score) /. (float @@ l)

let greedy_aux input l =
  let sats = input.sats in
  let state = Array.make (Array.length input.sats) Sparse.empty in
  List.fold_left (greedy_col input.max_time sats state) (M.empty, 0) l

let greedy input =
  let l = List.sort (fun c c' -> compare (score c') (score c)) @@ Array.to_list input.collections in
  let m, _ = greedy_aux input l in
  Format.printf "%d@." (M.cardinal m);
  M.iter (fun img (sat, round) ->
      Format.printf "%d %d %d %d@." img.lat img.long round sat) m

let rec try_aux file input score_max iter =
  match iter () with
  | None -> ()
  | Some l ->
    let m, score = greedy_aux input l in
    if score > score_max then begin
      let f = Format.sprintf "%s_%d" file score in
      let ch = open_out f in
      let fmt = Format.formatter_of_out_channel ch in
      Format.fprintf fmt "%d@." (M.cardinal m);
      M.iter (fun img (sat, round) ->
          Format.fprintf fmt "%d %d %d %d@." img.lat img.long round sat) m;
      try_aux file input score iter
    end else
      try_aux file input score_max iter

let subset l =
  let tmp = ref l in
  (fun () ->
     try
     let res = !tmp in
     tmp := List.tl res;
     Some res
     with Failure _ -> None)
let try_greedy file input =
  let l = List.sort (fun c c' -> compare (score c') (score c)) @@ Array.to_list input.collections in
  try_aux file input 0 (subset l)

