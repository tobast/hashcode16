
type t = {
  max_time : int;
  sats : Sat.t array;
  collections : Col.t array;
}

type imgLoc = {
	loc : Pos.t ;
	colId : int ; (* Collection id *)
	imgId : int ; (* Image id in the collection *)
}

type byLoc = imgLoc array (* Sorted by incr lat, incr long if lat equal *)

let id x = x
let get_int () = Scanf.bscanf Scanf.Scanning.stdin " %d" id

let dir = Pos.{ lat = 0; long = 0; }

let read_pos _ =
  let lat = get_int () in
  let long = get_int () in
  Pos.{ lat; long; }

let read_sat id =
  let pos = read_pos () in
  let speed = get_int () in
  let w = get_int () in
  let d = get_int () in
  Sat.{ id; pos; speed; dir; w; d; }

let read_range _ =
  let start = get_int () in
  let stop = get_int () in
  Col.{ start; stop; }

let read_collection _ =
  let score = get_int () in
  let n = get_int () in
  let m = get_int () in
  let imgs = Array.init n read_pos in
  let ranges = Array.init m read_range in
  Col.{ score; imgs; ranges; }

let read_pb () =
  let max_time = get_int () in
  let n = get_int () in
  let sats = Array.init n read_sat in
  let m = get_int () in
  let collections = Array.init m read_collection in
  { max_time; sats; collections; }

