
type t = {
  id : int;
  pos : Pos.t;
  speed : int;
  dir : Pos.t;
  w : int;
  d : int;
}

let deg = 60 * 60

let mmod a b =
  ((a mod b) + b) mod b

let longitude n =
  mmod (n + 180 * deg) (360 * deg) - 180 * deg

let next_position sat =
  let c = sat.pos.Pos.lat + sat.speed in
  if -90 * deg <= c && c <= 90 * deg then
	{ sat with
	  pos = {
		Pos.lat = c ;
		Pos.long = longitude (sat.pos.Pos.long - 15)
	  }
	}
  else if c > 90 * deg then
	{ sat with
	  pos = {
		Pos.lat = 180 * deg - c ;
		Pos.long = longitude (180 * deg + sat.pos.Pos.long - 15)
	  } ;
	  speed = -sat.speed
	}
  else
	{ sat with
	  pos = {
		Pos.lat = -180 * deg - c ;
		Pos.long = longitude (180 * deg + sat.pos.Pos.long - 15)
	  } ;
	 speed = -sat.speed
	}

let mdiv a b =
  if a < 0 then
	(a - b + 1) / b
  else
	a / b
	  
let n_turns sat n =
  let c = sat.pos.Pos.lat + n * sat.speed + 90 * deg in
  let mm = mmod c (360 * deg) in
  if mm <= 180 * deg then
	{ sat with
	  pos = {
		Pos.lat = mm - 90 * deg ;
		Pos.long = longitude (sat.pos.Pos.long - 15 * n)
	  }
	}
  else
	{ sat with
	  pos = {
		Pos.lat = 270 * deg - mm ;
		Pos.long = longitude (sat.pos.Pos.long - 15 * n + 180 * deg)
	  } ;
	  speed = -sat.speed
	}
	  
let rec n_pos_debug sat n =
  if n = 0 then sat
  else n_pos_debug (next_position sat) (n - 1)

let rec steps_intervalle start incr imin imax mm =
  if incr < 0 then
	steps_intervalle (mm - start) (-incr) (mm - imax) (mm - imin) mm
  else
	let (imin, imax) = if start >= imin
	  then (imin + mm, imax + mm)
	  else (imin, imax) in
	(imin - start + incr - 1) / incr

let steps_rect incrx incry minx miny maxx maxy mmx mmy maxtime =
  let in_int a mn mx =
	if mn < mx then mn <= a && a <= mx
	else mn <= a || a <= mx
  in
  let rec loop u v k =
	if in_int u minx maxx && in_int v miny maxy then
	  k
	else if k > maxtime then maxtime + 42
	else
	  let k1 = steps_intervalle u incrx minx maxx mmx in
	  let k2 = steps_intervalle v incry miny maxy mmy in
	  let k3 = min k1 k2 in
	  loop (mmod (u + k3 * incrx) mmx) (mmod (v + k3 * incry) mmy) (k + k3)
  in loop 0 0 0

let next_sight sat minlat minlong maxlat maxlong maxtime =
  let u = sat.pos.Pos.lat + 90 * deg in
  let v = sat.pos.Pos.long + 180 * deg in
  let mm = 360 * deg in
  let minlat = mmod (minlat + 90 * deg) mm in
  let maxlat = mmod (maxlat + 90 * deg) mm in
  let minlong = mmod (minlong + 180 * deg) mm in
  let maxlong = mmod (maxlong + 180 * deg) mm in
  let k1 = steps_rect sat.speed (-15)
	(mmod (minlat - u) mm) (mmod (minlong - v) mm)
	(mmod (maxlat - u) mm) (mmod (maxlong - v) mm)
	mm mm maxtime in
  let k2 = steps_rect sat.speed (-15)
	(mmod (mm - maxlat - u) mm) (mmod (180 * deg + minlong - v) mm)
	(mmod (mm - minlat - u) mm) (mmod (180 * deg + maxlong - v) mm)
	mm mm maxtime in
  min k1 k2
	  
(*
let next_sight sat minlat minlong maxlat maxlong maxtime =
  (* Format.eprintf "next_sight: %d %d %d %d %d %d %d@."
	sat.pos.Pos.lat sat.pos.Pos.long sat.speed
	minlat minlong maxlat maxlong
	; *)
  let minlong = mmod (minlong + 180 * deg) (360 * deg) - 180 * deg in
  let maxlong = mmod (maxlong + 180 * deg) (360 * deg) - 180 * deg in
  (* Format.eprintf "next_sight: %d %d %d %d %d %d %d@."
	sat.pos.Pos.lat sat.pos.Pos.long sat.speed
	minlat minlong maxlat maxlong
	; *)
  let in_int a mn mx =
	if mn < mx then mn <= a && a <= mx
	else mn <= a || a <= mx
  in
  let rec loop s k =
	(* Format.eprintf "%d %d@." sat.pos.Pos.lat sat.pos.Pos.long; *)
	if in_int s.pos.Pos.lat minlat maxlat && in_int s.pos.Pos.long minlong maxlong then
	  k
	else if k > maxtime then maxtime + 42
	else loop (next_position s) (k + 1)
  in
  let r = loop sat 0 in
  (* Format.eprintf "Result %d@." r; *)
  r

*)
