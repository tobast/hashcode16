
type range = {
  start :int ;
  stop : int;
}

type t = {
  score : int;
  imgs : Pos.t array;
  ranges : range array;
}

let in_range t r = r.start <= t && t <= r.stop

let in_ranges t a = CCArray.exists (in_range t) a


