
open Input
open Sat
open Col
open Pos


(* Simulate the position of each satellite turn by turn, and take every possible photo, greedily.
   Here we take pictures asap, but we will be able to do better sometimes (wait a bit so we don't have to change orientation too much) *)

type satpos = {
  sorient_last : Pos.t;
  sorient_time : int
}


let in_range r  t =
  t <= r.stop && t >= r.start

let in_ranges rs t =
  let n = Array.length rs in
  let rec scan i =
    if i = n then false
    else in_range rs.(i) t || scan (i+1)
  in
  scan 0


(* Return either Some(sorient) or None *)
let can_take_picture sat satpos imgpos time =
  let {lat = sx; long = sy} = sat.pos in
  let {lat = lx; long = ly} = imgpos in
  let (ox, oy) = (lx - sx, ly - sy) in
  (* In bounds ? *)
  let bound_strict = sat.w / 2 in
  let in_bounds = abs ox <= bound_strict && abs oy <= sat.d in
  let {lat = ox_old; long =  oy_old} = satpos.sorient_last in
  let dt = time - satpos.sorient_time in
  let can_sweep = abs (ox - ox_old) <= dt * sat.w && abs (oy - oy_old) <= dt * sat.w in
  if in_bounds && can_sweep then Some({lat = ox; long = oy}) else ((* Format.eprintf "Sorry :(@."; *) None)

(* A better scan says 'this is the last picture taken, after blabla' -> order 2 on satellites! *)

let greedy_scan imgMap inp =
  (* At each timestep *)
  (* Look at the position of the satellites, and where they were before *)
  let sats = Array.copy inp.sats in
  let nsats = Array.length sats in
  let satst = Array.map (fun s -> {sorient_last = s.dir; sorient_time = 0 }) sats in
  (* At each turn, list pictures that could be taken. We will do some collection exclusion / maximisation
     later.  *)
  let htaken = Hashtbl.create 17 in
  let evlist = ref [] in
  let need_take i iid _ _ =
    not (Hashtbl.mem htaken (i,iid))
  in
  let pic_taken i iid ipos time j =
    Hashtbl.add htaken (i,iid) ();
    evlist := (ipos, time,j) :: !evlist
  in
  let rec turn time =
    if time >= inp.max_time then ()
    else begin
        Format.eprintf "Turn %d@." time;
        (* Try to take pictures *)
        let rec scan j =
          if j = nsats then ()
          else
            
    let n = Array.length inp.collections in
    let rec smile i =
      if i = n then ()
      (* For collection in time range;
         for each picture in the collection;
         try to take the picture ! *)
      else if in_ranges inp.collections.(i).ranges time

      then begin
          let rec scan j =
            if j = nsats then ()
            else
              let {lat = px; long = py} = sats.(j).pos in
              let d = sats.(j).d in
              let candidates = prefilter_interval imgMap (px - d) (py - d) (px + d) (py + d) in
              List.iter (fun 
        let pics = inp.collections.(i).imgs in
        Array.iteri (fun iid ipos ->
                    if need_take i iid ipos time then
                    let rec scan j =
                      if j = nsats then ()
                      else match can_take_picture sats.(j) satst.(j) ipos time with
                           | None -> scan (j+1)
                           | Some ornew -> pic_taken i iid ipos time j;
                                           satst.(j) <- {satst.(j) with sorient_time = time};
                                           satst.(j) <- {satst.(j) with sorient_last = ornew}
                    in scan 0
                   ) pics;
        smile (i+1)
      end
      else smile (i+1)
    in
    smile 0;
    (* Update positions *)
    Array.iteri (fun i j -> sats.(i) <- next_position j) sats;
    turn (time + 1)
      end
  in
  turn 0;
  let dump_out () =
    Format.printf "%d\n" (List.length !evlist);
    List.iter (fun ({lat=x;long=y},time,sat) -> Format.printf "%d %d %d %d\n" x y time sat) (List.rev !evlist)
  in
  dump_out ()
