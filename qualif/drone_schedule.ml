
open Input
open Helpers
open Output

module IHeap = Heap.Make(struct
  type t = int * int
  let le = ( <= )
end)

let count_items =
  Array.fold_left (fun a b -> a + if b <> 0 then 1 else 0) 0

let get_items a =
  let l = ref [] in
  Array.iteri (fun i x -> if x <> 0 then l := (i, x) :: !l) a;
  !l
  
let scheduler input next_order =
  let drone_positions = Array.make input.drones input.warehouses.(0).src in
  (* let heap = ref IHeap.empty in *)
  let free_time = Array.make input.drones 0 in
  (* for i = 0 to input.drones - 1 do
	heap := IHeap.insert (0, i) !heap
	 done; *)
  let best_drone pos =
	let best = ref 0 in
	let bestdist = ref max_int in
	for i = 0 to input.drones - 1 do
	  if free_time.(i) + distance drone_positions.(i) pos < !bestdist then begin
		best := i;
		bestdist := free_time.(i) + distance drone_positions.(i) pos
	  end
	done;
	!best
  in
  let commands = ref [] in
  let rec loop () = match next_order () with
	  | None -> List.rev !commands
	  | Some (wr, ord) ->
		 (* let (time, drone), new_heap = IHeap.extract_min !heap in *)
		 let drone = best_drone wr.src in
		 let time = free_time.(drone) in
		 (* heap := new_heap; *)
		 let wr_pos = wr.src in
		 let dest = ord.dest in
		 let cpos = drone_positions.(drone) in
		 drone_positions.(drone) <- dest;
		 let tt = 2 * (count_items ord.order) + distance wr_pos cpos
		   + distance wr_pos dest in
		 if tt + time > input.turns then loop ()
		 else begin
		   (* heap := IHeap.insert (tt + time, drone) !heap; *)
		   free_time.(drone) <- tt + time;
		   let items = get_items ord.order in
		   let pick = List.map (fun (item, count) ->
			 (drone, Load (wr.id, item, count))
		   ) items in
		   let deliver = List.map (fun (item, count) ->
			 (drone, Deliver (ord.id, item, count))
		   ) items in
		   commands := deliver @  pick @ !commands;
		   loop ()
		 end
  in loop ()
