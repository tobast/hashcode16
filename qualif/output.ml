
type command =
  | Unload of int * int * int
  | Load of int * int * int
  | Deliver of int * int * int
  | Wait of int

type output = (int * command) list

let print_command ff command =
  match command with
  | Load (warehouse, product, amount) ->
	 Format.fprintf ff "L %d %d %d" warehouse product amount
  | Unload (warehouse, product, amount) ->
	 Format.fprintf ff "U %d %d %d" warehouse product amount
  | Deliver (warehouse, product, amount) ->
	 Format.fprintf ff "D %d %d %d" warehouse product amount
  | Wait time ->
	 Format.fprintf ff "W %d" time

let print_output ff out =
  Format.fprintf ff "%d\n" (List.length out);
  List.iter (fun (drone, command) ->
	Format.fprintf ff "%d %a\n" drone print_command command) out

module IHeap = Heap.Make(struct
  type t = int * int * command
  let le (x, _, _) (y, _, _) = x <= y end)

let rec filtermap f l =
  match l with
  | [] -> []
  | x :: t -> match f x with
	| None -> filtermap f t
	| Some u -> u :: (filtermap f t)
  
let check commands input =
  let input = Input.copy input in
  let score = ref 0 in
  let n = input.Input.drones in
  let cmds = Array.make n [] in
  let new_cmds = ref [] in
  List.iter (fun (i, cmd) ->
	cmds.(i) <- cmd :: cmds.(i)) (List.rev commands);
  let load = Array.make n 0 in
  let ml = input.Input.max_load in
  let remaining = Array.map (fun order ->
	Array.fold_left (+) 0 order.Input.order
  ) input.Input.orders in
  let wr = input.Input.warehouses in
  let ord = input.Input.orders in
  let we = input.Input.weights in
  let positions = Array.make n wr.(0).Input.src in
  let drone_inv = Array.make_matrix n (Array.length we) 0 in
  let wr_pos w = wr.(w).Input.src in
  let action_move i cmd = match cmd with
	| Load (w, _, _) | Unload (w, _, _) -> begin
	   let d = Helpers.distance positions.(i) (wr_pos w) in
	   positions.(i) <- wr_pos w;
	   d
	end
	| Deliver (d, _, _) -> begin
	  let pos = ord.(d).Input.dest in
	  let d = Helpers.distance positions.(i) pos in
	  positions.(i) <- pos;
	  d
	end
	| Wait n -> n - 1
  in
  let heap = ref IHeap.empty in
  for i = 0 to n - 1 do
	match cmds.(i) with
	| [] -> ()
	| cmd :: rest -> begin
	  let et = action_move i cmd in
	  heap := IHeap.insert (et, i, cmd) !heap;
	  (if et + 1 <= input.Input.turns then
		  new_cmds := (i, cmd) :: !new_cmds);
	  cmds.(i) <- rest
	end
  done;
  while not (IHeap.is_empty !heap) do
	let (time, drone, cmd), new_heap = IHeap.extract_min !heap in
	let drones = ref [cmd, drone] in
	heap := new_heap;
	while not (IHeap.is_empty !heap) &&
	    (let (t, _, _) = IHeap.min !heap in t = time) do
	  let (_, drone, cmd), new_heap = IHeap.extract_min !heap in
	  heap := new_heap;
	  drones := (cmd, drone) :: !drones
	done;
	(* Make sure all unloads are before all loads *)
	let actions = List.sort compare !drones in
	List.iter (fun (cmd, drone) ->
	  match cmd with
	  | Load (warehouse, product, amount) -> begin
		assert (amount > 0);
		assert (wr.(warehouse).Input.items.(product) >= amount);
		assert (load.(drone) + amount * we.(product) <= ml);
		wr.(warehouse).Input.items.(product) <-
		  wr.(warehouse).Input.items.(product) - amount;
		load.(drone) <- load.(drone) + amount * we.(product);
		drone_inv.(drone).(product) <-
		  drone_inv.(drone).(product) + amount;
	  end
	  | Unload (warehouse, product, amount) -> begin
		assert (amount > 0);
		assert (drone_inv.(drone).(product) >= amount);
		wr.(warehouse).Input.items.(product) <-
		  wr.(warehouse).Input.items.(product) + amount;
		load.(drone) <- load.(drone) - amount * we.(product);
		drone_inv.(drone).(product) <-
		  drone_inv.(drone).(product) - amount
	  end
	  | Deliver (o, product, amount) -> begin
		assert (amount > 0);
		assert (ord.(o).Input.order.(product) >= amount);
		assert (drone_inv.(drone).(product) >= amount);
		ord.(o).Input.order.(product) <-
		  ord.(o).Input.order.(product) - amount;
		load.(drone) <- load.(drone) - amount * we.(product);
		remaining.(o) <- remaining.(o) - amount;
		drone_inv.(drone).(product) <-
		  drone_inv.(drone).(product) - amount;
		if (remaining.(o) = 0) then begin
		  (* SCORE, la commande est complétée à time + 1 *)
		  let tt = input.Input.turns in
		  score := !score +
			((tt - (time + 1)) * 100 + tt - 1) / tt
		end
	  end
	  | Wait n -> ()
	) actions;
	List.iter (fun (_, i) ->
	  match cmds.(i) with
	  | [] -> ()
	  | cmd :: rest -> begin
		let et = time + 1 + action_move i cmd in
		heap := IHeap.insert (et, i, cmd) !heap;
		(if et + 1 <= input.Input.turns then
			new_cmds := (i, cmd) :: !new_cmds);
		cmds.(i) <- rest
	  end
	) actions
  done;
  !score, List.rev !new_cmds

let read_out_file () =
  let get_char () = Scanf.bscanf Scanf.Scanning.stdin "%c " (fun x -> x) in
  Input.(
  let n = get_int () in
  let r = ref [] in
  for _ = 1 to n do
	let drone = get_int () in
	let cmd = match get_char () with
	  | 'L' -> let a = get_int () in
			   let b = get_int () in
			   let c = get_int () in
			   Load (a, b, c)
	  | 'U' -> let a = get_int () in
			   let b = get_int () in
			   let c = get_int () in
			   Unload (a, b, c)
	  | 'D' -> let a = get_int () in
			   let b = get_int () in
			   let c = get_int () in
			   Deliver (a, b, c)
	  | 'W' -> let a = get_int () in
			   Wait a
	  | _ -> assert false
	in
	r := (drone, cmd) :: !r
  done;
  List.rev !r)
