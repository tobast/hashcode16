
type point = {
  row : int;
  col : int;
}

type warehouse = {
  id : int;
  src : point;
  items : int array;
}

type order = {
  id : int;
  dest : point;
  order : int array;
  mutable todo : bool;
}

type t = {
  max_row : int;
  max_col : int;
  drones : int;
  max_load : int;
  turns : int;

  weights : int array;
  warehouses : warehouse array;
  orders : order array;
}

let id x = x

let get_int () = Scanf.bscanf Scanf.Scanning.stdin "%d " id

let get_point () = { row = get_int (); col = get_int (); }

let copy t =
  { t with
    weights = Array.copy t.weights;
    warehouses = Array.map (fun w ->
        { w with items = Array.copy w.items }) t.warehouses;
    orders = Array.map (fun o ->
        { o with order = Array.copy o.order }) t.orders;
  }

let read_pb () =
  let max_row = get_int () in
  let max_col = get_int () in
  let drones = get_int () in
  let turns = get_int () in
  let max_load = get_int () in
  let p = get_int () in
  let weights = Array.init p (fun _ -> get_int ()) in
  let w = get_int () in
  let warehouses = Array.init w (fun id ->
      let src = get_point () in
      let items = Array.init p (fun _ -> get_int ()) in
      { id; src; items; })
  in
  let c = get_int () in
  let orders = Array.init c (fun id ->
      let dest = get_point () in
      let n = get_int () in
      let order = Array.make p 0 in
      for _ = 1 to n do
        let i = get_int () in
        order.(i) <- order.(i) + 1
      done;
      { id; dest; order; todo = true; })
  in
  { max_row; max_col; drones; max_load; turns;
    weights; warehouses; orders; }

