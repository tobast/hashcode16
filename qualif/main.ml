
module H = Helpers
module O = Output

let () =
  let t = Input.read_pb () in
  let l = Drone_schedule.scheduler t (Meaw.greedy t) in
  Output.print_output Format.std_formatter l

(*
let () =
  let t = Input.read_pb () in
  let (tu,dr,wh,ord,it) = t.turns, t.drones, Array.length t.warehouses, Array.length t.orders, Array.length t.weights in
  Format.eprintf "Turns: %d, drones: %d, warehouses: %d, orders: %d, items: %d\nEstimated complexity: %d\n" tu dr wh ord it (dr*wh*ord*it);(*
  let sol = Greedy.do_something t in
                                                                                                                                             Format.printf "%a" Output.print_output sol *) ()

*)

