
let () =
  let input = Input.read_pb () in
  let cmds = Output.read_out_file () in
  let score, new_cmds = Output.check cmds input in
  Printf.eprintf "Score : %d\n" score;
  Format.printf "%a" Output.print_output new_cmds
