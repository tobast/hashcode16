
open Input

let distance { row = x; col = y } { row = x'; col = y' } =
  let x2 = (x - x') * (x - x') in
  let y2 = (y - y') * (y - y') in
  int_of_float (ceil (sqrt (float_of_int (x2 + y2))))

	
