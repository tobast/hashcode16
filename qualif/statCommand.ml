(***
 * Compiler ce fichier (ocamlbuild) et lui donner à manger sur stdin le
 * fichier de test
 ***)

open Input

let problem = read_pb ()
let dLoad = problem.max_load

type stats = {
	sumCapacity : float ;
	capacities : float list
}

let orderWei ord =
	let out = ref 0 in
	Array.iteri (fun i item -> out := !out + item * problem.weights.(i)) ord;
	!out
let orderCap ord =
	(float_of_int (orderWei ord)) /. (float_of_int dLoad)

let out = Array.fold_left (fun prev order ->
	let curCap = orderCap order.order in
	{
		sumCapacity = prev.sumCapacity +. curCap ;
		capacities = curCap :: prev.capacities
	}) { sumCapacity = 0. ; capacities = [] } problem.orders

let avg = (out.sumCapacity /. (float_of_int (List.length out.capacities)))
let sorted = List.sort compare out.capacities
let med = List.nth sorted ((List.length sorted) / 2)
let below1 = List.fold_left (fun prev x -> if x <= 1. then prev+1 else prev)
		0 sorted
let belowHalf = List.fold_left (fun prev x -> if x <= 0.5 then prev+1 else prev)
		0 sorted
let below1per = (float_of_int below1) /. (float_of_int (List.length sorted))
let belowHalfper =
	(float_of_int belowHalf) /. (float_of_int (List.length sorted))

let _ = Format.printf "Average: %f\nMed: %f\nBelow 1: %f\nBelow half: %f@."
		avg med below1per belowHalfper

