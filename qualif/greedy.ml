(* Greedy algorithm, test *)

open Input
open Helpers
open Output

let dist = distance

  
exception Complete
  
let first_item o =
  let n = Array.length o in
  let rec first i =
    if i = n then raise Complete
    else if o.(i) <> 0 then i
    else first (i+1)
  in
  first 0

let optimize a f =
  let n = Array.length a in
  if n = 0 then raise Not_found;
  let rec opti best besti i =
    if i = n then (best,besti)
    else let sci = f a.(i) in
         if sci > best
         then opti sci i (i+1)
         else opti best besti (i+1)
  in
  opti (f a.(0)) 0 1;
  List.map foo bar baz

let count_obj a = Array.fold_left (fun k x -> k + min x 1) 0 a

let blit_array a b =
  assert (Array.length a = Array.length b);
  Array.iteri (fun i x -> b.(i) <- x) a
let array_map2 f a b =
  assert (Array.length a = Array.length b);
  Array.mapi (fun i x -> f x b.(i)) a

let array_for_all f a =
  let n = Array.length a in
  let rec fall i =
    if i = n then true
    else f a.(i) && fall (i+1)
  in
  fall 0

let knapsack_greedy i items max_load =
  let n = Array.length items in
  let a = Array.make n 0 in
  let rec fill fw j =
    if j = n then max_load - fw
    else
      (* Try to fit as much of the item as possible ! *)
      let jw = i.weights.(j) in
      a.(j) <-  min items.(j) (fw / jw);
      fill (fw - a.(j) * jw) (j+1)
  in
  let w = fill max_load 0 in
  (w,a)

let knapsack_best i items _ =
  let l = Binpack.binpack  {order = items; dest = {row = 0; col = 0}; id = 0; todo = false} i in
  match l with
  | x::_ -> x.order
  | [] -> [||]

let fake_order items = {order = items; dest = {row = 0; col = 0}; id = 0; todo = false}
(* Knapsack ! Today I feel greedy *)
let knapsack = knapsack_best

let gen_stuff f a =
  let b = ref [] in
  Array.iteri (fun it q ->
    if q <> 0
    then b := (f it q)::!b ;
  ) a;
  !b

let gen_loads j wh at =
  gen_stuff (fun it q -> (j, Load(wh,it,q))) at

    

let gen_delivers j ord at =
  gen_stuff (fun it q -> (j, Deliver(ord,it,q))) at


let do_something i =
  let weight o =
    let w = ref 0 in
    Array.iteri (fun it q -> w := !w + q*i.weights.(it)) o;
    !w
  in

  let availability = Array.make i.drones 0 in
  let positions = Array.make i.drones i.warehouses.(0).src in
  let opti_command cid c =
    let av' = Array.copy availability in
    let pos' = Array.copy positions in
    let wh' = Array.map (fun wh -> {wh with items = Array.copy wh.items}) i.warehouses in
    let dest = c.dest in
    (* Just find the first available drone and deliver in order *)
    let actions = ref [] in
    let deliver_most j o =
       (* Find closest warehouse with available items *)
      optimize (Array.mapi (fun i w -> (i,w)) wh') (fun (whid,wh) ->
        let potential = array_map2 min wh.items o in
        (*        Array.iteri (fun i q -> Format.printf "(%d, %d)" i q) wh.items; *)
        let attrib = knapsack i potential i.max_load in
        let weight = weight attrib in
(*        Format.printf "%d@." whid;
        Array.iteri (fun i q -> Format.printf "(%d, %d)" i q) attrib; 
          exit 0; *)
        let nobj = count_obj attrib in
        let stop = (av'.(j) + dist pos'.(j) wh.src + 2 * nobj + dist wh.src dest) in
        if stop > i.turns || nobj = 0 then ( None)
        else Some (-stop, weight,  attrib)
      )
    in
    (* Now try to find the best drone *)
    let deliver_most_all o =
      optimize (Array.init i.drones (fun i -> i)) (fun j ->
        deliver_most j o
      )
    in
    let rec fullfill o ctime=
      match deliver_most_all o with
      | ((Some (mav,_,attrib)), wh), j  -> begin
        Array.iteri (fun i x -> o.(i) <- o.(i) - x) attrib;
        Array.iteri (fun i x -> assert (wh'.(wh).items.(i) >= x); wh'.(wh).items.(i) <- wh'.(wh).items.(i) - x) attrib;
        pos'.(j) <- dest;
        av'.(j) <- -mav;
        (* Generate loads, generate unloads *)
        first_item attrib;
        actions := !actions @ gen_loads j wh attrib @ gen_delivers j cid  attrib;
        match first_item o with
        | _ -> fullfill o (max mav ctime)
        | exception Complete -> Some (ctime, av', pos',wh',!actions)
      end
      | (None, _), _ -> None
    in

    (*     Format.eprintf "Got a solution for command %d@." cid; *)
    match fullfill (Array.copy c.order) 0 with
    | Some((_,_,_,_,actions) as k) -> assert (actions <> []); Some k
    | None -> None
  in
  (* Mark completed commands ? *)
  let sc = Array.make (Array.length i.orders) false in
  let commands = ref [] in
  (* TODO : early stop ! *)
  let early_stop () =
    array_for_all (fun x -> x >= i.turns) availability in
  let rec complete_best () =
    let tot = ref 0 in
    Array.iter (Format.eprintf "%d ") availability;
    Format.eprintf " (TURNS: %d)@." i.turns;
    match optimize (Array.init (Array.length i.orders) (fun i -> i)) (fun cid ->
      if sc.(cid) then None
      else Some((opti_command cid i.orders.(cid))
    )) with
    | Some (Some (_,av',pos',wh',actions)),cid ->
       incr tot;
       Format.eprintf "Delivered command %d (tot = %d)@." cid !tot;
      sc.(cid) <- true;
      let cl = List.length !commands in
      commands := !commands @ actions;
      assert (List.length !commands > cl);
       blit_array av' availability;
       blit_array pos' positions;
       blit_array wh' i.warehouses;
       complete_best ()
    | _ -> !commands
  in
  (* Cost of an order == weight *)
  let heuristic_moaw o dest = 2 * count_obj o + 2 * (List.length (Binpack.binpack (fake_order o) i)) * dist i.warehouses.(0).src dest in
  (* Find the best way to fullfil an order *)
  Random.self_init ();
  let heuristic_other o =
    List.length (Binpack.binpackWarehouse i o) in
  let rec complete_weight () =
    let orders = Array.mapi (fun i o -> (heuristic_other o, Random.bits (), i)) i.orders in
    Array.sort compare orders;
    let n = Array.length orders in
    let commands = ref [] in
    let tot = ref 0 in
    let rec doit cpos =
      if cpos = n then !commands
      else
        let (_,_,cid) = orders.(cpos) in
        match opti_command cid i.orders.(cid) with
      | Some (_,av',pos',wh',actions) ->
         incr tot;
        Format.eprintf "Delivered command %d (tot = %d)@." cid !tot;
        sc.(cid) <- true;
        let cl = List.length !commands in
        commands := !commands @ actions;
        assert (List.length !commands > cl);
        blit_array av' availability;
        blit_array pos' positions;
        blit_array wh' i.warehouses;
        doit (cpos+1)
      | _ -> doit (cpos+1)
    in
    doit 0
  in(*
  let rec complete_weight_n () =
    let orders = Array.mapi (fun i o -> ((* heuristic_moaw *)weight o.order (* o.dest *), i)) i.orders in
    Array.sort compare orders;
    let n = Array.length orders in
    let commands = ref [] in
    let tot = ref 0 in
    let rec doit cpos =
      if cpos = n then !commands
      else
        let (_,cid) = orders.(cpos) in
        match opti_command cid i.orders.(cid) with
      | Some (_,av',pos',wh',actions) ->
         incr tot;
        Format.eprintf "Delivered command %d (tot = %d)@." cid !tot;
        sc.(cid) <- true;
        let cl = List.length !commands in
        commands := !commands @ actions;
        assert (List.length !commands > cl);
        blit_array av' availability;
        blit_array pos' positions;
        blit_array wh' i.warehouses;
        doit (cpos+1)
      | _ -> doit (cpos+1)
    in
    doit 0
  in
    *)
  complete_weight ()
