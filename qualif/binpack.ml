(**
 * Détermine le nombre minimal de drones pour une liste d'items, et comment les
 * remplir.
 **)

open Input

let rec mkList cur a = function
  | 0 -> cur
  | n -> mkList (a::cur) a (n-1)

type bin = {
  stuff : int array ;
  mutable remCap : int
}

exception OrderDone

let binpackWarehouse (instance : t) (itemsIn : order) : (int * order) list =
  let locWh = Array.init (Array.length instance.warehouses)
      (fun k -> Array.copy instance.warehouses.(k).items) in
  let nbTypes = Array.length instance.weights in

  let items = Array.copy (itemsIn.order) in

  let whFilter wh = Array.init nbTypes (fun k -> min items.(k) (locWh.(wh).(k))) in
  let bestWarehouse () =
    let whWei wh =
      let ar = whFilter wh in
      let out = ref 0 in
      Array.iteri (fun i x -> out := !out + x * instance.weights.(i)) ar;
      int_of_float ((float_of_int !out) /.
	  	(float_of_int (Helpers.distance itemsIn.dest instance.warehouses.(wh).src)))
    in
    let rec mkList_out out = function
      | 0 -> out
      | n -> mkList_out ((n-1, whWei (n-1))::out) (n-1)
    in
    let out = mkList_out [] (Array.length locWh) in
    let sOut = List.sort (fun a b -> (snd b - snd a)) out in

    if snd (List.hd sOut) = 0 then
      raise OrderDone
    else
      (fst (List.hd sOut))
  in

  let fillDrone wh =
    let avail = whFilter wh in
    let itemsList = ref [] in
    Array.iteri (fun i x ->
        itemsList := (mkList [] i x) @ (!itemsList)) avail;
    let sItems = List.sort
        (fun a b -> (instance.weights.(b) - instance.weights.(a)))
        !itemsList in
    let free = ref (instance.max_load) in
    let outOrder = {
      id = itemsIn.id ;
      dest = itemsIn.dest ;
      order = Array.make (Array.length itemsIn.order) 0 ;
      todo = false } in
    List.iter (fun it ->
        if instance.weights.(it) <= !free then begin
          free := !free - instance.weights.(it);
          outOrder.order.(it) <- outOrder.order.(it) + 1;
		  items.(it) <- items.(it) - 1 ;
          locWh.(wh).(it) <- locWh.(wh).(it)
        end) sItems;
    outOrder
  in

  let rec doOrders cur = try (
    let wh =bestWarehouse () in
    doOrders ((wh, fillDrone wh) :: cur)
  )
    with OrderDone ->
      cur
  in

  doOrders []

let binpack (itemsIn : order) (instance : t) : order list =
  let items = ref [] in
  Array.iteri (fun i k ->
      items := (mkList [] i k) @ (!items)) (itemsIn.order);
  let sItems = List.sort (fun a b ->
      let weiA = instance.weights.(a) and weiB = instance.weights.(b) in
      if weiA < weiB then
        1
      else if weiA = weiB then
        0
      else
        -1
    ) !items in

  let bins = List.fold_left (fun prev item ->
      let wei = instance.weights.(item) in
      let rec findDeepest = function
        | [] -> false
        | h :: t ->
          let out = findDeepest t in
          (match out with
           | true -> true
           | false->
             if h.remCap >= wei then (
               h.stuff.(item) <- h.stuff.(item) + 1;
               h.remCap <- (h.remCap) - wei ;
               true
             ) else
               false
          )
      in
      (match findDeepest prev with
       | true -> prev
       | false ->
         let nStuff = Array.make (Array.length (instance.weights)) 0 in
         nStuff.(item) <- 1 ;
         { stuff = nStuff ;
           remCap = (instance.max_load - wei) } :: prev
      ))
      [] sItems
  in

  List.fold_left (fun prev x ->
      {
        id = itemsIn.id;
        dest = itemsIn.dest ;
        order = x.stuff ;
        todo = true
      } :: prev) [] bins


let testBinpack weights =
  let inst = {
    max_row = 0;
    max_col = 0;
    drones = 0;
    max_load = 100;
    turns = 0;
    weights = Array.init 101 (fun k -> k);
    warehouses = Array.make 0 {src={row=0;col=0}; items=Array.make 0 0;id=0};
    orders = Array.make 0 { id = 0; dest = {row=0;col=0}; order=Array.make 0 0;todo=false} } in

  let rec count out x = function
    | [] -> out
    | h::t -> count (if x=h then (out+1) else out) x t
  in

  let ord = {
    id = 0;
    order =  Array.init 101 (fun k -> count 0 k weights) ;
    dest = {row=0;col=0} ;
    todo = true } in
  let out = binpack ord inst in

  Format.printf "%d@." (List.length out)
