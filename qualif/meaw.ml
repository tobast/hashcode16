
open Input

(* TODO: change this function *)
let bin_pack = Binpack.binpackWarehouse

let score l =
  List.fold_left (fun acc (w, o) -> acc + Helpers.distance w.src o.dest) 0 l

let check w o =
  let b = ref true in
  for i = 0 to Array.length w.items - 1 do
    b := !b && w.items.(i) >= o.order.(i)
  done;
  !b

let take w o =
  for i = 0 to Array.length w.items - 1 do
    w.items.(i) <- w.items.(i) - o.order.(i);
  done

let greedy t =
  let orders = ref (Array.to_list t.orders) in
  let tmp = ref [] in
  let rec aux () =
    match !tmp with
    | ((w, o) as h) :: r ->
      tmp := r;
      assert (check w o);
      take w o;
      Some h
    | [] ->
      if !orders = [] then None
      else
        let l = List.map (fun o ->
            let tmp = bin_pack t o in
            let res = List.map (fun (i, o') -> (t.warehouses.(i), o')) tmp in
            (o, res)) !orders in
        let o, subs = List.hd (List.sort (fun (_, x) (_, y) ->
            Pervasives.compare (score x) (score y)) l) in
        Format.eprintf "Order %d selected@." o.id;
        orders := List.filter (fun o' -> not (o == o')) !orders;
        tmp := subs;
        aux ()
  in
  aux

